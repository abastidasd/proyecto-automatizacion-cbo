#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Validacion PopUp
  El usuario debe poder ingresar al formulario los datos requeridos.
  Cada campo del formulario realiza validaciones, longitud y formato,
  el sistema debe presentar las validaciones respectivas.

  @CasoExitoso
  Scenario: Diligenciamento exitoso del formulario
    Given Autentico en colorlib con usuario "demo" y clave "demo"
    And Ingreso a la funcionalidad Forms Validation 
    When Diligencio Formulario Popup validation
    |Required|Select|MultipleS1|MultipleS2|Url|Email|Password1|Password2|Minsize|Maxsize|Number|IP|Date|DataEarlier|
    |01|Golf|Tennis|Golf|http://www.andrea.com|andrea@hotmail.com|andrea|andrea|123456|123456|1234|200.200.5.4|2019-09-20|2012/8/17|
    Then Verifico ingreso exitoso
    
 @CasoAlterno
  Scenario: Diligenciamento con errores del formulario Popup Validation.
  					se presenta Globo Informativo indicando error en el diligenciamiento de alguno de los campos.
    Given Autentico en colorlib con usuario "demo" y clave "demo"
    And Ingreso a la funcionalidad Forms Validation 
    When Diligencio Formulario Popup validation
    |Required|Select|MultipleS1|MultipleS2|Url|Email|Password1|Password2|Minsize|Maxsize|Number|IP|Date|DataEarlier|
    ||Golf|Tennis|Golf|http://www.andrea.com|andrea@hotmail.com|andrea|andrea|123456|123456|1234|200.200.5.4|2019-09-20|2012/8/17|
    |01|Choose a sport|Tennis|Golf|http://www.andrea.com|andrea@hotmail.com|andrea|andrea|123456|123456|1234|200.200.5.4|2019-09-20|2012/8/17|
   
    Then Verificar que se presente Globo Informativo de validación
