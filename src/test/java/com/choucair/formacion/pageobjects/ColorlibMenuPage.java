package com.choucair.formacion.pageobjects;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

import org.openqa.selenium.WebElement;

public class ColorlibMenuPage extends PageObject {
	
	//Menu forms
		@FindBy (xpath="//*[@id=\'menu']/li[6]")
		public WebElement menuForms;
				
		
		//subMenu form General
			@FindBy (xpath="//*[@id=\'menu\']/li[6]/ul/li[1]")
			public WebElement menuFormGenerals;
			
			//subMenu form Validation
			@FindBy (xpath="//*[@id=\'menu\']/li[6]/ul/li[2]")
			public WebElement menuFormValidation;
					
					
		//form validation - lable informativo
			@FindBy (xpath="//*[@id=\'content\']/div/div/div[1]/div/div/header")
			public WebElement lblFormValidation;
								
			
			public void menuFormValidation() {
				menuForms.click();
				menuFormValidation.click();
				String strMensaje = lblFormValidation.getText();
				assertThat(strMensaje, containsString("Popup Validation"));
			}		
				
			

}
